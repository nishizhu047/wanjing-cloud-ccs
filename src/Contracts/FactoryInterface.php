<?php

namespace Wanjing\Ccs\Contracts;

interface FactoryInterface
{
    public function config(array $config): self;

    public function create(int $type): ProviderInterface;

    public function getResolvedProviders(): array;

    public function buildProvider(string $provider, array $config): ProviderInterface;
}
