<?php

namespace Wanjing\Ccs\Contracts;

interface ProviderInterface
{
    public function send(string $to, string $from): string;

    public function callNotify(array $data) : array;

    public function getDownloadUrl(string $url) : string;
}
