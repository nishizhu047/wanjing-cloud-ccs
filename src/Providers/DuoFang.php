<?php

namespace Wanjing\Ccs\Providers;

use Wanjing\Ccs\Exceptions\Exception;

/**
 * @see http://agent.duofangtongxin.com/#/help/help
 */
class DuoFang extends Base
{
    protected array $headers = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->headers = ['headers' => ['Content-Type' => 'application/json']];
    }

    /**
     * 发起呼叫
     * @param string $to
     * @param string $from
     * @param string $x
     * @return string
     * @throws Exception
     */
    public function send(string $to, string $from,string $x = ''): string
    {
        $data = [
            'accessKey' => $this->config['accountSid'],
            'telA' => $from,
            'telB' => $to,
            'telX' => $this->config['x'] ?? '',
            'expire' => 30,
            'notifyUrl' => $this->config['notifyUrl'],
            'notifyData' => $this->config['type'],
        ];
        $data['sign'] = $this->sign($data);

        return $this->request('/open/api-call/bind',$data,$this->headers)['data']['bindId'] ?? '';
    }


    public function callNotify(array $data): array
    {
        if ($data['pushType']){
            return [
                'type' => $data['pushType'],
                'fileUrl' => $data['recordUrl'] ?? '',
                'sendId' => $data['bindId'],
            ];
        }
        $msg = '主叫挂断';
        $code = 10002;
        if ($data['finishStatus'] == 1) {
            $msg = '被叫挂断';
            $code = 8002;
        }elseif ($data['finishStatus'] == 2) {
            $msg = '主叫放弃';
            $code = 7001;
        }elseif ($data['finishStatus'] == 3) {
            $msg = '被叫放弃';
            $code = 8002;
        }elseif ($data['finishStatus'] == 4) {
            $msg = '其它';
            $code = 9999;
        }
        return [
            'type' => $data['pushType'],
            'duration' => $data['duration'],
            'fileUrl' => $data['preRecordUrl'] ?? '',
            'from' => $data['telA'],
            'to' => $data['telB'],
            'x' => $data['telX'],
            'code' => $code,
            'start' => $data['startTime'],
            'end' => $data['endTime'],
            'sendId' => $data['bindId'],
            'message' => $msg
        ];
    }

    public function getDownloadUrl(string $url): string
    {
        return $url;
    }

    /**
     * 签名
     * @param array $map
     * @return string
     */
    protected function sign(array $map): string
    {
        $sb = $this->config['authToken'];
        ksort($map);

        foreach ($map as $key => $value) {
            if (!empty($value)) {
                $sb .= $key . $value;
            }
        }
        return strtoupper(md5($sb));
    }
}
