<?php

namespace Wanjing\Ccs\Providers;

use Wanjing\Ccs\Exceptions\Exception;

class ShuQi extends Base
{
    const SHUQI_STATUS = [
        0 => '正常',
        100 => '放音收号时系统接收到的号码为空，若要进行收号，请在语音通知内容播放完毕后的5秒之内进行按键操作',
        501 => '录音创建会议失败',
        502 => '录音创建会议超时',
        503 => '录音主叫加入会议失败',
        504 => '录音主叫加入会议超时',
        505 => '录音增加录音端点失败',
        506 => '录音增加录音端点超时',
        507 => '启动录音失败',
        508 => '启动录音超时',
        509 => '放录音提示音失败',
        510 => '放录音提示音超时',
        512 => '录音VCU倒换释放呼叫',
        513 => '呼叫超时',
        514 => '振铃超时',
        515 => '远端用户主动Cancel',
        516 => '本端用户主动Cancel',
        517 => '平台响应超时',
        518 => '远端用户呼叫失败，请结合Q850原因值分析',
        519 => '本端用户呼叫失败(OXX)，请结合Q850原因值分析',
        520 => '本端用户呼叫失败(18X)，请结合Q850原因值分析',
        521 => '远端呼叫还未振铃已经拆线',
        522 => '语音端口不足',
        524 => '放音文件不存在',
        526 => '静音失败',
        527 => '取消静音失败',
        528 => '限制呼叫非特定前缀的号码',
        529 => 'appkey不存在',
        530 => 'forward前主叫挂机',
        539 => '录音流程未完成，主叫主动挂断电话',
        540 => '录音流程未完成，被叫主动挂断电话',
        541 => '被叫接通后和主叫的重协商失败',
        542 => '录音超时释放呼叫',
        550 => '本端呼叫振铃失败',
        551 => '点击呼叫本端收到OXX消息',
        552 => '远端振铃时本端用户挂机',
        553 => '远端还未振铃时本端用户挂机',
        400 => '699 在对端没有返回Q850原因值时，直接用SIP消息码(如404：  NOT FOUND)',
        1001 => '坐席主动挂机',
        1002 => '用户主动挂机',
        1003 => 'SP调用总机释放接口挂机',
        1004 => '发起点击外呼后，坐席进行盲转并挂机',
        2801 => 'IVR放音失败',
        2802 => 'IVR规则无效',
        2803 => 'IVR按键超过最大次数',
        2805 => 'IVR不在服务时间-1',
        2806 => 'IVR不在服务时间-2',
        2810 => '未配置MRF',
        2811 => '未配置ICSCF',
        2812 => '放音超时',
        2825 => '满意度调查结束-1  (配置满意度结束音)',
        2829 => '拨分机号被叫忙或者无应答',
        2831 => '企业呼入License受限',
        2837 => '放音失败',
        2838 => 'INVITE超时释放',
        2847 => '总机选线无可用坐席',
        2853 => '坐席不可用，拒接呼入',
        2863 => '留言创建录音失败',
        2868 => '企业已停机',
        2871 => '动态ivr等待sp指令超时',
        2901 => '~ 2914 录音失败',
        7001 => '开发者呼叫频次管控',
        7002 => '应用呼叫频次管控',
        7003 => '显示号码呼叫频次管控',
        7004 => '被叫黑名单呼叫管控',
        7005 => '主叫黑名单呼叫管控',
        7100 => 'app信息不存在',
        7105 => '业务类型不匹配',
        7106 => 'APP未申请个人小号能力',
        7107 => '安全管控提示音放音失败',
        7108 => '用户状态已冻结',
        7109 => '语音端口不足',
        8000 => '内部错误',
        8001 => '固话号码呼叫频次管控|被叫关机/无应答/用户正忙等：请核实被叫手机终端是否处于正常状态',
        8002 => '接续用户时对端返回失败放音',
        8003 => '用户振铃超时',
        8004 => '用户振铃时挂机',
        8005 => 'TTS转换失败',
        8006 => '放音文件不存在',
        8007 => '给用户放音失败',
        8008 => '给用户放音收号失败，若要进行收号，请在语音通知内容播放完毕后的5秒之内进行按键 操作',
        8009 => '接通前主叫用户主动挂机',
        8010 => '超过通话最大时长挂机',
        8011 => '内部错误',
        8012 => '无效的app_key',
        8015 => '给用户录音失败',
        8017 => '客户指示挂机',
        8018 => '业务无权限',
        8020 => '业务异常',
        8100 => '被叫号码不存在',
        8101 => '被叫无应答',
        8102 => '被叫用户正忙（被叫正在通话、振铃等）',
        8103 => '被叫用户暂时无法接通',
        8104 => '被叫已关机（被叫处于关机、飞行模式、无网络等状态）',
        8105 => '被叫挂机或被叫已停机',
        8106 => '主叫号码不存在',
        8107 => '主叫无应答',
        8108 => '主叫用户正忙（主叫正在通话、振铃等）',
        8109 => '主叫用户暂时无法接通',
        8110 => '主叫已关机（主叫处于关机、飞行模式、无网络等状态）',
        8111 => '主叫挂机或主叫已停机',
    ];

    /**
     * 发起呼叫
     * @param string $to
     * @param string $from
     * @return string
     * @throws Exception
     */
    public function send(string $to, string $from): string
    {
        $url = $this->config['base_url']."/bdsaas/phone/callPhone.do";

        $data = [
            'companyCode' => $this->config['appId'],  //公司代码
            'userName' => $from,  //坐席数企登陆账号
            'password' => $this->config['password'],  //坐席数企登陆密码
            'toPhone' => $to, //被叫号码
            'ip' => '115.60.184.213', //拨打电话的客户端的ip地址
        ];

        return $this->request($url,$data)['data'];
    }

    /**
     * 创建坐席，此平台需要人工创建坐席，返回待审核
     * @param string $mobile
     * @return int
     * @throws Exception
     */
    public function createSeat(string $mobile): array
    {
        $data = [
            'companyCode' => $this->config['appId'],  //公司代码
            'userName' => $mobile,  //坐席数企登陆账号
            'password' => $this->config['password'],  //坐席数企登陆密码
            'seatPhone' => $mobile, //需要删除坐席的手机号
        ];
        $this->request('/bdsaas/phone/addPhoneSeat.do',$data);
        return ['status' => 1];
    }

    public function getDownloadUrl(string $url): string
    {
        return $url;
    }

    public function smsGet(string $mobile, string $code): void
    {
        // TODO: Implement smsGet() method.
    }

    public function smsSend(string $mobile, int $type = 0): void
    {

    }

    /**
     * 删除坐席
     * @param string $mobile
     * @return void
     * @throws Exception
     */
    public function deleteSeat(string $mobile): void
    {
        $data = [
            'companyCode' => $this->config['appId'],  //公司代码
            'userName' => $mobile,  //坐席数企登陆账号
            'password' => $this->config['password'],  //坐席数企登陆密码
            'seatPhone' => $mobile, //需要删除坐席的手机号
        ];
        $this->request('/bdsaas/ajax/badu/callAgency/phone/deletePhoneSeat.do',$data);
    }

    /**
     * 获取指定话单详情
     * @param string $sendId
     * @return array
     * @throws Exception
     */
    public function get(string $sendId): array
    {
        $data = [
            'companyCode' => $this->config['appId'],  //公司代码
            'userName' => $this->config['mobile'],  //坐席数企登陆账号
            'password' => $this->config['password'],  //坐席数企登陆密码
            'sessionIdListJson' => $sendId, //唯一标识符sessionId的ListJson字符串(sessionId为拨打电话接口返回值)
        ];
        return $this->request('/bdsaas/phone/queryCallPhoneRecord.do',$data);
    }

    public function stateNotify(array $data): array
    {
        // TODO
        return [

        ];
    }

    public function callNotify(array $data): array
    {
        // TODO
        return [
            'duration' => $data['timeConsume'],
            'fileUrl' => $data['recordUrl'] ?? '',
            'from' => str_replace('+86','',$datum['callerNum'] ?? ''),
            'to' => str_replace('+86','',$datum['calleeNum'] ?? ''),
            'code' => $code['code'] ?? 9999,
            'start' =>  $data['connectTime'] ?? null,
            'end' => $data['disconnectTime'] ?? null,
            'sendId' => $data['sessionId'],
            'message'=> $code['msg'] ?? '其它原因'
        ];
    }

    public function change(string $old,string $new,int $type = 0): void
    {

    }

    public function rechargeApp(float $amount) : void
    {

    }
}
