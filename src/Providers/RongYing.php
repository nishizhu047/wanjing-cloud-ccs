<?php

namespace Wanjing\Ccs\Providers;

use Wanjing\Ccs\Exceptions\Exception;

class RongYing extends Base
{
    const RONGYING_STATUS = [
        'HangUp' => ['code' => 0, 'msg' => '正常'],
        'Busy' => ['code' => 552, 'msg' => '主叫用户忙'],
        'CallerBusy' => ['code' => 7001, 'msg' => '主叫未接（正忙/拒接/未接/拦截）'],
        'CallerNoAnswer' => ['code' => 7002, 'msg' => '主叫异常（关机/停机/无服务）'],
        'CallerFailure' => ['code' => 7003, 'msg' => '呼叫中主叫挂机'],
        'CallerAbandon' => ['code' => 7004, 'msg' => '被叫忙'],
        'CalleeBusy' => ['code' => 7108, 'msg' => '被叫无应答'],
        'CalleeNoAnswer' => ['code' => 8001, 'msg' => '被叫异常（空号/关机/停机/无服务）'],
        'CalleeFailure' => ['code' => 8002, 'msg' => '被叫未接（正忙/拒接/未接/拦截）'],
        'UNALLOCATED_NUMBER' => ['code' => 10001, 'msg' => '被叫号码错误'],
        'ORIGINATOR_CANCEL' => ['code' => 10002, 'msg' => '主叫挂断'],
        'NO_USER_RESPONSE' => ['code' => 10003, 'msg' => '被叫未接通或正忙'],
        'Other' => ['code' => 9999, 'msg' => '其它原因'],
    ];

    protected array $headers = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->headers = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => base64_encode($this->config['appId'].':'.$this->config['authToken'].':'.date('YmdHis')),
            ]];
    }

    /**
     * 发起呼叫
     * @param string $to
     * @param string $from
     * @return string
     * @throws Exception
     */
    public function send(string $to, string $from): string
    {
        $sign = $this->sign();

        $data = [
            'AccountSid' => $this->config['accountSid'],  //账号授权令牌
            'Appid' => $this->config['appId'],  //应用id
            'Caller' => $from,  //主叫号码只能是座席绑定的电话号码
            'Callee' => $to, //被叫号码
            'MaxDuration' => $this->config['maxTalkTime'] ?? 0, //此次通话最大时长 （分钟）
            'LastMinVoice' => '对不起，您还能通话 1 分钟',//最后一分钟放音提 示音
            'LastMinToUE' => 'toa', //最后一分钟放音对 象
        ];

        return $this->request("/20200305/rest/click/call/event/v1?sig=".$sign,$data,$this->headers)['SessionId'];
    }

    /**
     * 创建坐席
     * @param string $mobile
     * @return array
     * @throws Exception
     */
    public function createSeat(string $mobile): array
    {
        $data = $this->request('/rest/CreateSeatAccount/v4',['BindNumber' => $mobile,'Name' => $mobile,'Tsid' => 1070, 'Type' => 0],$this->headers);
        return ['status' => 1,'name' => $data['SeatAccount'] ?? $data['seatPhone']];
    }

    /**
     * 坐席短信下发
     * @param string $mobile
     * @param int $type
     * @return void
     * @throws Exception
     */
    public function smsSend(string $mobile,int $type = 0): void
    {
        $this->request('/20200625/rest/send/reset/v1',[
            'Caller' => $mobile, //绑定号码，必须是已绑定坐席号码，否则验证不过，且 保证与 Appid 一一对应
            'Type' => $type,//短信下发类型，默认使用验证码上行验证，需与坐席验 证短信上行配合使用；同时也可使用，直接下发短信， 采用回复短信的形式验证；
        ],$this->headers);
    }

    /**
     * 坐席短信上行
     * @param string $mobile
     * @param string $code
     * @return void
     * @throws Exception
     */
    public function smsGet(string $mobile,string $code): void
    {
        $this->request('/20200625/rest/send/reset/v2',[
            'Caller' => $mobile, //绑定号码，必须是已绑定坐席号码，否则验证不过，且 保证与 Appid 一一对应
            'Code' => $code,//短信验证码
        ],$this->headers);
    }

    /**
     * 删除坐席
     * @param string $mobile
     * @return void
     * @throws Exception
     */
    public function deleteSeat(string $mobile): void
    {
        $this->request('/20200305/rest/click/call/event/v1',['Appid' => $this->config['appId'],'SeatAccount' => $mobile],$this->headers);
    }

    /**
     * 获取指定话单详情
     * @param string $sendId
     * @return array
     * @throws Exception
     */
    public function get(string $sendId): array
    {
        return $this->request('/20200306/rest/click/call/record/v1',['CallDetail' => ['SessionId' => $sendId]],$this->headers);
    }

    /**
     * 更改坐席号码
     * @param string $old
     * @param string $new
     * @param int $type
     * @return void
     * @throws Exception
     */
    public function change(string $old,string $new,int $type = 0): void
    {
        $this->request('/rest/changeBindNumber/v2',[
            'OldNumber' => $old,
            'NewNumber' => $new,
            'Tsid' => 1070,//座席验证私有短信签名，需要单独找商务申请开 通，申请成功后即可直接使用，如果该值无效则 默认发送平台签名（预留参数）
            'Type' => $type, //0：回复短信验证（默认）； 1：验证码上行
        ],$this->headers);
    }

    /**
     * 应用余额转入
     * @param float $amount
     * @return void
     * @throws Exception
     */
    public function rechargeApp(float $amount) : void
    {
        $data = [
            'Appid' => $this->config['appId'],
            'Banlance' => $amount,
        ];
        $this->request("/20200306/rest/appid/tocharge/into/v1",$data);
    }

    public function stateNotify(array $data): array
    {
        return [
            'status' => $data['eventType'],
            'from' => str_replace('+86','',$datum['caller'] ?? ''),
            'to' => str_replace('+86','',$datum['called'] ?? ''),
            'code' => 0,
            'message'=> ''
        ];
    }

    public function callNotify(array $data): array
    {
        $code = self::RONGYING_STATUS[$data['ulFailReason']];
//        if ($code == 8001 && strpos($records[$datum['sessionId']]['course'],'被叫振铃')) $callStatus = 8002;
        return [
            'duration' => $data['billsec'],
            'fileUrl' => $data['recordFileDownloadUrl'] ?? '',
            'from' => str_replace('+86','',$datum['calleeNum'] ?? ''),
            'to' => str_replace('+86','',$datum['fwdDstNum'] ?? ''),
            'code' => $code['code'] ?? 9999,
            'start' =>  $data['recordFileDownloadUrl'] ? date('Y-m-d H:i:s',strtotime($data['recordFileDownloadUrl']) + 8 * 3600) : null,
            'end' => $data['callEndTime'] ? date('Y-m-d H:i:s',strtotime($data['callEndTime']) + 8 * 3600) : null,
            'sendId' => $data['sessionId'],
            'message'=> $code['msg'] ?? '其它原因'
        ];
    }

    public function getDownloadUrl(string $url): string
    {
        return $url;
    }

    /**
     * 签名
     * @return string
     */
    protected function sign(): string
    {
        // 主帐号Id+主帐号授权令牌+时间戳
        return strtoupper(md5(sprintf('%s:%s:%s',$this->config['authToken'],$this->config['appId'],date('YmdHis'))));
    }
}
