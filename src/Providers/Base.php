<?php

namespace Wanjing\Ccs\Providers;

use App\Model\Ccs\ErrorModel;
use Hyperf\Context\ApplicationContext;
use Hyperf\Redis\Redis;
use Swlib\Saber;
use Wanjing\Ccs\Constant\DuoFangCode;
use Wanjing\Ccs\Constant\JiuYunCode;
use Wanjing\Ccs\Contracts;
use Wanjing\Ccs\Exceptions\Exception;

abstract class Base implements Contracts\ProviderInterface
{
    protected array $config;

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    abstract public function send(string $to, string $from): string;

    abstract public function callNotify(array $data) : array;

    abstract public function getDownloadUrl(string $url) : string;

    /**
     * @throws Exception
     */
    public function request(string $url, array $data, array $headers = [])
    {
        $to = $data['telB'] ?? ($data['req']['telB'] ?? '');
        // 判断号码是否为手机号或座机号
        if (!preg_match('/^1[3456789]\d{9}$/', $to) && !preg_match('/^0\d{2,3}-?\d{7,8}$/', $to)) {
            throw new Exception('号码验证失败，请检查被叫号码');
        }
        $saber = Saber::create(['timeout' => 10,'use_pool' => true]);
        try {
            $result = $saber->post($this->config['base_url'].$url,$data,$headers);
        }catch (\Exception $e){
            $this->error($e->getMessage(),$data,$this->config['base_url'].$url,[],$headers);
            throw new Exception('请求运营商失败，请稍后再试');
        }

        $result = json_decode($result->getBody()->getContents(), true);
        // if(isset($data['telA']) && $data['telA'] == ''){
        //     var_dump($result);
        //     var_dump($data);
        // }

//        if ($this->config['type'] == 1){
//            if (!isset($result['Flag']) || $result['Flag'] != 200){
//                throw new Exception('呼叫失败：'.($result['Msg'] ?? '未知错误，请稍后再试'));
//            }
//        }else if($this->config['type'] == 2){
//            if (!isset($result['rspCode']) || $result['rspCode'] != 0){
//                throw new Exception($result['rspMsg'] ?? '未知错误，请稍后再试');
//            }
//        }else
        if($this->config['type'] == 3){
            if (!isset($result['resp']['statusCode']) || $result['resp']['statusCode'] != '000000') {
                $this->error(
                    $result['resp']['represent'] ?? ('未知错误：'.$result['resp']['statusCode']),
                    $data
                    ,$this->config['base_url'].$url,$result,$headers
                );
                if (str_contains($result['resp']['represent'] ?? '','被叫区域限制')) throw new Exception('被叫呼叫盲区，请更换被叫号码！');
                if (str_contains($result['resp']['represent'] ?? '','主叫未加白')) throw new Exception('主叫号码已失效，请联系客服！');
                $msg = explode('##',$result['resp']['represent'])[2] ?? '请更换号码或稍后再试';
                throw new Exception($msg);
            }
        }else {
            if (!isset($result['code']) || $result['code'] != '0000') {
//                $this->releasePhonePool($data['telX']);
                $this->error($result['message'] ?? '请更换号码或稍后再试',$data,$this->config['base_url'].$url,$result,$headers);

                $msg = DuoFangCode::CODE[$result['code']] ?? preg_replace('/\[.*?\]/', '', str_replace(['A号码','B号码','X'],['主叫','被叫','中间号'],strtolower($result['message'] ?? '请更换号码或稍后再试')));
                if (str_contains($msg,'in the blacklist') || str_contains($msg,'bnumber is in a local blacklist')) throw new Exception('被叫是黑名单用户，请更换被叫号码！');
                if (str_contains($msg,'Phone state hit the blacklist')) throw new Exception('被叫是黑名单用户，请更换被叫号码！');
                if (str_contains($msg,'monthly call restriction') || str_contains($msg,'Exceeding The limit')) throw new Exception('被叫本月被呼上限，请更换被叫号码！');
                if (str_contains($msg,'daily call restriction')) throw new Exception('被叫今日被呼上限，请更换被叫号码！');
                if (str_contains($msg,'high-risk users are restricted from calling')) throw new Exception('被叫是高风险用户，请更换被叫号码！');
                if (str_contains($msg,'加白') || str_contains($msg,'白名单')) throw new Exception('主叫号码已失效，请联系客服重新审核！');
                if (str_contains($msg,'每月通话次数限制')) throw new Exception('被叫本月被呼上限，请更换被叫号码！');
                if (str_contains($msg,'the dialed call is too short')) throw new Exception('主叫与被叫的通话间隔太短，请更换被叫号码');
                if (str_contains($msg,'余额')) throw new Exception('线路维护中请稍后再试');
                if (preg_match("/[\x{4e00}-\x{9fa5}]+/u", $msg)) throw new Exception($msg);
                throw new Exception('线路繁忙，请重试');
            }
        }
        return $result;
    }

    public function error(string $msg, array $data,string $uri = '',array $result = [],array $headers = [])
    {
        $error = new ErrorModel();
        $error->to = $data['telB'] ?? $data['req']['telB'];
        $error->from = $data['telA'] ?? $data['req']['telA'];
        $error->middle = $data['telX'] ?? '';
        $error->error = $msg;
        $error->uri = $uri;
        $error->param = empty($data) ? null : json_encode($data,JSON_UNESCAPED_UNICODE);
        $error->result = empty($result) ? null : json_encode($result,JSON_UNESCAPED_UNICODE);
        $error->headers = empty($headers) ? null : json_encode($headers,JSON_UNESCAPED_UNICODE);
        $error->save();
    }
}
