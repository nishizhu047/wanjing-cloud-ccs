<?php

namespace Wanjing\Ccs\Providers;

use Wanjing\Ccs\Exceptions\Exception;

/**
 * @see https://www.33e9cloud.com/index.php?m=home&c=View&a=index&aid=175
 */
class JiuYun extends Base
{
    protected array $headers = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->headers = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => base64_encode($this->config['accountSid'].':'.date('YmdHis')),
            ]];
    }

    /**
     * 发起呼叫
     * @param string $to
     * @param string $from
     * @return string
     * @throws Exception
     */
    public function send(string $to, string $from): string
    {
        $sign = $this->sign();
        $url = "/{$this->config['softVersion']}/Accounts/{$this->config['accountSid']}/MaskNumberAX/doubleCall?sig=".$sign;

        $data = [
            'req' => [
                'appId' => $this->config['appId'],
                'telA' => $from,
                'telB' => $to,
                'userData' => $this->config['type'],
            ]
        ];

        return $this->request($url,$data,$this->headers)['resp']['subid'];
    }


    public function callNotify(array $data): array
    {
        return [
            'type' => 0,
            'duration' => $data['duration'],
            'fileUrl' => $data['recordUrl'] ?? '',
            'from' => str_replace('+86','',$data['caller'] ?? ''),
            'to' => str_replace('+86','',$data['called'] ?? ''),
            'x' => $data['telX'],
            'code' => $data['duration'] > 0 ? 0 : 9999,
            'start' => date('Y-m-d H:i:s',strtotime($data['startTimeCallee'])),
            'end' => date('Y-m-d H:i:s',strtotime($data['endTime'])),
            'sendId' => $data['subid'],
            'message'=> '完成'
        ];
    }

    public function getDownloadUrl(string $url): string
    {
        if (empty($url) && !filter_var($url, FILTER_VALIDATE_URL)) return '';
        $sign = strtoupper(md5($this->config['accountSid'] .':'. $this->config['authToken']));
        $url .= ("auth=".$sign);
        return $url;
    }

    /**
     * 签名
     * @return string
     */
    protected function sign(): string
    {
        // 主帐号Id+主帐号授权令牌+时间戳
        return strtoupper(md5($this->config['accountSid'] . $this->config['authToken'] . date('YmdHis')));
    }
}
