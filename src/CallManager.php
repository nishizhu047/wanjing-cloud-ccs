<?php

namespace Wanjing\Ccs;

use JetBrains\PhpStorm\Pure;
use Wanjing\Ccs\Contracts\FactoryInterface;

class CallManager implements FactoryInterface
{
    protected array $config;

    protected array $resolved = [];

    protected const PROVIDERS = [
        1 => Providers\RongYing::class,
        2 => Providers\ShuQi::class,
        3 => Providers\JiuYun::class,
        4 => Providers\DuoFang::class,
    ];

    #[Pure]
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    public function config(array $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function create(int $type): Contracts\ProviderInterface
    {
        if (! isset($this->resolved[$type])) {
            $this->resolved[$type] = $this->createProvider($type);
        }

        return $this->resolved[$type];
    }

    public function getResolvedProviders(): array
    {
        return $this->resolved;
    }

    public function buildProvider(string $provider, array $config): Contracts\ProviderInterface
    {
        $instance = new $provider($config);

        $instance instanceof Contracts\ProviderInterface || throw new Exceptions\InvalidArgumentException("The {$provider} must be instanceof ProviderInterface.");

        return $instance;
    }

    /**
     * @throws Exceptions\InvalidArgumentException
     */
    protected function createProvider(int $type): Contracts\ProviderInterface
    {
        $config = array_merge(config('ccs.' . $type, []), $this->config);
        $config['type'] = $type;
        return $this->buildProvider(self::PROVIDERS[$type], $config);
    }
}
